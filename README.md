RecTec Grill telemetry & control with Node-RED
==============================================

Are you a techie nerd who likes to smoke meat?  Welcome to the club!  I own a RecTec grill and wanted to get status and temperature data as well as control it from my local home automation system.

This guide is not so much a guide as a pointer to a couple of repositories I used nearly out of the box.

Disclaimer
==========

I am not affiliated with the RecTec Grills company in any way and am in no way responsible if you somehow destroy your grill.  This is just documentation of what I did.

Caveats
-------

* This method of control means you will not be able to use the RecTec app to control your grill again until you re-pair your grill specifically with the RecTec app.  You cannot use this method and the RecTec app at the same time.
* It has been [commented](https://github.com/SDNick484/rectec_status/issues/3#issuecomment-589976193) that even if you complete these steps and block outbound internet access to the grill, it may stop responding until you re-link it again.  Your mileage may vary and I will update this as I do more cooks.
* Update 2020-06-01: It's been several months and I have not seen any issues with the grill failing to respond and having to re-link.  Smoke on!

Why?
----

I'm a nerd that likes all the data I can get.  I love my smoker and the fact that it's "smart".  I like that it has an app and gives me a graph and all, but the graph is a bit lacking and I want to save the data and incorporate the grill into my self-built home automation system.   

The RecTec grill uses a controller developed around a software called [Tuya](https://www.tuya.com/).  This is a complete package which provides developers an API as well as cloud service.  Some folks (such as myself) would rather not have their devices communicating with the cloud, or just simply want to integrate them.  That is the main purpose for "hacking" the grill.

Equipment
---------

* RecTec RT-590 "Stampede" (purchased Nov 2019, though this has been tested on some other models)
* A PC capable of running Node-RED (many folks use a Raspberry Pi, I used a docker instance on a server in my house)
* A PC/laptop running some flavor of linux with a wifi adapter (this is required for the linking process to work)

Software
--------

* [Node-RED](https://nodered.org/) - Flow based programming tool (I use for home automation)
* [node-red-contrib-tuya-local](https://github.com/subzero79/node-red-contrib-tuya-local) - Node for controlling Tuya devices (the grill)
* (optional) Database software of your choice (I like [postgresql](https://www.postgresql.org/) but many folks use [influxdb](https://www.influxdata.com/)
* (optional) Node-RED contrib to go with your database software of choice
* (optional) [Grafana](https://grafana.com/) - Awesome for visualizing data and setting alarms

Knowledge
---------

You should probably know your way around linux software and configuring computers and such if you're going to try this.  You also should read the disclaimer at the top saying I'm merely documenting what I did and am in no way repsonsible for what you do to your grill.

Steps
-----

1. From your linux PC/laptop with a wifi adapter, read carefully and run [these steps](https://github.com/codetheweb/tuyapi/blob/master/docs/SETUP.md#linking-a-tuya-device-with-smart-link). Be sure to note the values you used in step 5 and values you get back in step 6.
2. Install Node-RED (docs [here](https://nodered.org/docs/getting-started/))
3. Install the tuya-local node (docs [here](https://github.com/subzero79/node-red-contrib-tuya-local#installing)).  I use docker for Node-RED and simply added the `npm install` to my `Dockerfile`.
4. Optionally install any databases and/or Node-RED nodes which go along with them, and Grafana if you wish to use.
5. Open up Node-RED (typically `http://yourserver:1880`) and drag a new `tuya local` node from the left pane into the flow (grid).
6. Double-click the `tuya local` and enter the following:   
   a. `Device name` - whatever you want to call this node (I called it `RecTec`)   
   b. `IP Address` - IP address of the grill   
   c. `ID` - the device ID (should have noted from above)   
   d. `Key` - the local key (should have noted from above)   
   e. `Version` - this is likely either `3.1` or `3.3`.  Newer grills likely have the newer version.  Try either one and see what works.   
   f. `Filter commandByte` - I left this blank   
   g. `Rename schema` - paste this JSON text in if you'd like to get friendly key/value pairs:   
```json
{
    "1": "power",
    "102": "set_temperature",
    "103": "actual_temperature",
    "104": "min_feedrate",
    "105": "food_temp1",
    "106": "food_temp2",
    "107": "temp_calibration",
    "109": "er1_alarm",
    "110": "er2_alarm",
    "111": "er3_alarm" 
}
```   

7. Now connect a debug node or other node you wish to recieve the data and click the `Deploy` button, you should then see your data!   
   
Example of what the flow might look like   
![alt text](rectec2.PNG)   
   
Grafana Example   
![alt text](rectec.PNG)